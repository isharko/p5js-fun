var barWidth    = 3;
var spacing     = 2;
var fft;
var soundFile;
var shiftX 		= -1000;
var momentum 	= 0;
var dest 		= 0;
var bars 		=[];
var radius 		= 50;

var exp3 = {
	fftBands 	: 450
}

function initExp3(newSoundFile){
	soundFile = newSoundFile;
	fft = new p5.FFT();	

	if (bars.length > 0) {return;}
	for (var i=0; i<exp3.fftBands; i++){
		 bars[bars.length] = new bar( i );
	}
	radius = windowHeight / 10;
}

function setMusic(){
}

function updateExp3() {
    background(0);
    frequencySpectrum = fft.analyze();
    drawExp3();
}

function bar(id){
	this.angle = 2.0 * PI / (exp3.fftBands-60) * (id);	
	this.offset = 0;
}


function drawExp3() {
	var d = shiftX - dest;
	// momentum -= d/10.0;
	// momentum *= 0.9;
	shiftX -= d / 20.0;
	translate(windowWidth/2, windowHeight/2);
	rotate(-PI/2);
	noStroke();
	var height = windowHeight/4;
	for (var i = 60; i < bars.length; i++){
		var x = map(i, 0, exp3.fftBands, 0, windowWidth);
		var h = map(frequencySpectrum[i], 0, 255, 1, height);
		var angle = bars[i].angle;
		// rotate(angle);
		var rad = radius + (h*1.2) + bars[i].offset;
		translate(rad*cos(angle)  ,rad*sin(angle));
		rotate(angle);
		fill(255);
		// if (i<50){ h/=2 };
		translate(-h, 0);
		rect(0,0,h,2);
		translate(h, 0);
		rotate(-angle);
		translate(-rad*cos(angle),-rad*sin(angle));
		if (h<10){
			bars[i].offset = 0;	
		}
		
		bars[i].offset += h*0.1;
	}
}

