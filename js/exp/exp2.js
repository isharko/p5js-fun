var fft;
var soundFile;
var shiftX = -1000;
var momentum = 0;
var dest = 0;

var exp2 = {
	fftBands 	: 512
}

function initExp2(newSoundFile){
	soundFile = newSoundFile;
	fft = new p5.FFT();
}

function setMusic(){
}

function updateExp2() {
    background(0);
    frequencySpectrum = fft.analyze();
    drawExp2();
}

function bar(){

}


function drawExp2() {
	var barWidth    = 2;
	var spacing     = 1;
	
	var d = shiftX - dest;
	// momentum -= d/10.0;
	// momentum *= 0.9;
	shiftX -= d / 20.0;

	noStroke();
	var height = windowHeight/4;
	for (var i = 0; i< exp2.fftBands; i++){
		var x = map(i, 0, exp2.fftBands, 0, windowWidth);
		var h = map(frequencySpectrum[i], 0, 255, 0, height);
		rect(x + shiftX  + spacing + barWidth * i, windowHeight/2 -h, barWidth, h*2) ;
	}
}

