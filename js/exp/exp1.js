var particleArray = [];
var stripeWidth = 60; // Here you may set max flackes to be created 


var angle = 10;
var angleSpeed = 1;
var nShapes = 21.0;
var sqWidth;
var momentum = 0.0;
var shapes = [];
var fft;

var exp1 = {
    angle : 0
}

function initExp1(){
	w = windowWidth / 2;
    h = windowHeight / 2;
    fft = new p5.FFT();
    for (var i = 0; i < nShapes; i++){
        shapes[shapes.length] = new shape(i);
    }
}

function updateExp1() {
    frequencySpectrum = fft.analyze();
    background(0);
    updateWidth();
    drawExp1();
}

function updateWidth(  ){
    for (var i = 0; i < nShapes; i++)  {
        var d = shapes[i].sqWidth + shapes[i].w;
        shapes[i].momentumX -= d / 3.0;
        shapes[i].momentumY -= d / 3.0;
        shapes[i].momentumX *= shapes[i].momentumDamp;
        shapes[i].momentumY *= shapes[i].momentumDamp;
        shapes[i].sqWidth += shapes[i].momentumX;
        var h = map(frequencySpectrum[i+20], 0, 255, 0, 255);
        shapes[i].sqWidth += -h/2;
        shapes[i].color = h;
        shapes[i].strokeWeight = map(h, 0, 100, 0, 1);

    }
}

function shape( id ){
    this.momentumX = id * 10.0;
    this.momentumY = 0;
    this.sqWidth = 500;
    this.w = random(windowWidth/10, windowWidth/5);
    this.momentumDamp = random(0.8, 0.85);
    this.color;
    this.strokeWeight;
}


function drawExp1() {
   
   translate(windowWidth/2, windowHeight/2);
   rotate(exp1.angle);
   stroke(255);
   noFill();
   for (var i = 0; i < nShapes; i++){
        rotate( 2 * PI / nShapes );        
        drawPentagon(i);
   }
   exp1.angle += 0.01;
}

function drawPentagon(i){

    for (j = 0; j <= PI * 2; j += 2.0*PI/5.0){
        stroke(shapes[i].color);
        strokeWeight(shapes[i].strokeWeight);
        var noiseScale = 0.001;
        var noiseVal = noise((mouseX+windowWidth/2)*noiseScale, mouseY*noiseScale);
        var x1 = cos(j) * (shapes[i].sqWidth);
        var y1 = sin(j) * (shapes[i].sqWidth);
        var x2 = cos(j+2.0*PI/5.0) * (shapes[i].sqWidth);
        var y2 = sin(j+2.0*PI/5.0) * (shapes[i].sqWidth);
        line(x1,y1,x2,y2);
    }
}

