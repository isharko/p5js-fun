var particleArray = [];
var maxparticles = 200; // Here you may set max flackes to be created 



function initExp0(){
    if (particleArray.length > 0) { return; }
	for (var i=0; i < maxparticles; i++){
      addparticle();
    };
}

function addparticle() {
    particleArray[particleArray.length] = new particle();
}

function updateExp0() {
    background(0);

    var m1 = 2.0;
    for (var i = 0; i < particleArray.length; i++) {
        //particle
        var p = particleArray[i];

        //distance to destination
        var dX = p.x - p.destinationX;
        var dY = p.y - p.destinationY;

        //momentum
        p.momentumX -= dX/p.width;
        p.momentumY -= dY/p.width;

        //momentum dim
        p.momentumX *= 0.8;
        p.momentumY *= 0.8;
        
        //dist to mousePos
        var dMX = mouseX - p.x;
        var dMY = mouseY - p.y;

        p.x += p.momentumX + dMX*dMX*dMX/(p.width * p.width * p.width + 10000000.0); // +momentum + funky mouse displacement
        p.y += p.momentumY + dMY*dMY*dMY/(p.width * p.width * p.width + 10000000.0); // +momentum + funky mouse displacement
    }

    drawExp0();
}

function particle() {
    this.x              = windowWidth/2;
    this.y              = windowHeight/2;
    //pick width
    if (Math.random()<0.2){ this.width      = (Math.random() * 40) + 1;    
    } else { this.width                     = (Math.random() * 10 ) + 1; }
    this.height                             = this.width;


    this.speed          = Math.round(Math.random() * 5) + 5.0;
    this.momentumX      = (Math.random()-0.5) *  this.width;
    this.momentumY      = (Math.random()-0.5) *  this.width;
    
    this.angle          = Math.random( ) * 2 * Math.PI;
    this.destinationX   = Math.cos(this.angle) * (90 + Math.random()*150.0) + windowWidth/2;
    this.destinationY   = Math.sin(this.angle) * (90 + Math.random()*150.0) + windowHeight/2;


    if (Math.random() < 0.5){   this.mColor = color(255,255,255);
    } else {                    this.mColor = color(200,200,200); }
}

function drawExp0() {

    for (var i = 0; i < particleArray.length; i++) {
        fill(particleArray[i].mColor);
        noStroke();
        ellipse(particleArray[i].x, particleArray[i].y, particleArray[i].width, particleArray[i].width);
        
        // bufferCanvasCtx.beginPath();
        // bufferCanvasCtx.arc(particleArray[i].x, particleArray[i].y, particleArray[i].width, 0, 2 * Math.PI, false);
        // bufferCanvasCtx.fill();
    }

    // drawLogo(bufferCanvasCtx);
}