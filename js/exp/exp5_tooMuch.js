var exp5 = {
    xspacing    : 16,
    theta       : 0.0,
    amplitude   : 75.0,
    period      : 500.0,   // How many pixels before the wave repeats
    yValues     : 0,
    nStrings    : 2,
    dx          : 0,
    ySpacing    : 30,
    nPoints     : 100,

}

var strings = [];
var waves   = [];

function initExp5(myTune){
    fft = new p5.FFT();
    // exp5.dx = (TWO_PI / exp5.period) * exp5.xspacing;
    exp5.xspacing = windowWidth / exp5.nPoints;    
    for (var i = 0; i < exp5.nStrings; i++){
        strings[i] = new string( i );
    }
}

//string
function string(i){
    this.id         = i;    
    this.y          = (i - exp5.nStrings/2) * exp5.ySpacing + windowHeight/2;
    this.points     = [];
    this.shock      = 0;
    this.shockDim   = 0.9;
    this.momentum   = 0;
    this.shocks     = [];

    for(var i = 0; i < exp5.nPoints; i++){
        var p = new _point();
        p.x = exp5.xspacing * i;
        this.points[i] = p;
    }

}

function _point(){
    this.y = 0;
    this.x = 0;
}

function shock(){
    this.size   = random(10, 50);
    this.speed  = 1;
    this.force  = 1;
    this.x      = 0;
    this.decay  = this.size / 100;
    // this.decay  = speed / 100;
}


//update
function updateExp5() {
    frequencySpectrum = fft.analyze();
    background(0);
    // for (var i=0; i<exp5.nStrings; i++){
        updateStrings();
    // }

    for (var i=0; i<waves.length; i++){
        if (waves[i].x > windowWidth){
            waves.splice(i, 1);
        }
    }

    drawExp5();
}

function updateStrings(){
    //cycle through each string
    for (var stringID = 0; stringID < strings.length; stringID++){
        //cycle through each effector "shock"
        for (var shockID = 0; shockID < strings[stringID].shocks.length; shockID++ ){
            var myShock = strings[stringID].shocks[shockID];            
            myShock.x += myShock.speed;
            myShock.size -= myShock.decay;
            if (myShock.size <= 0){
                removeShock(stringID, shockID);
            }
        }

        var mPoints = strings[stringID].points;
        //update points based on effectors
        for (var i = 0; i < strings[stringID].shocks.length; i++){
            // var s = strings[stringID].shocks[i];
            // var minX = floor(s.x - s.force * s.size);
            // var maxX = floor(s.x + s.force * s.size);
            for (var x = 0; x < mPoints.length; x++){
                if (mPoints[x] != null){                    
                    mPoints[x].y = random(-10,10);
                }
            }
        }

    }
}

function addShock(stringID, speed){
    var s = new shock();
    s.speed = speed;
    strings[stringID].shocks[strings[stringID].shocks.length] = s;
}
function removeShock(stringID, shockID){
    strings[stringID].shocks.splice(shockID, 1);
}


//draw
function drawExp5() {        
    drawStrings();
}

//draw string
function drawStrings(){
    stroke(255);

    //foreach string
    for (var stringID = 0; stringID < strings.length; stringID++){
        translate(0, strings[stringID].y);

        //foreach shock that affects the string
        for (var shockID = 0; shockID < strings[stringID].shocks.length; shockID++ ){
            var myShock = strings[stringID].shocks[shockID];
            translate(myShock.x, 0);
            if (stringID == 0){
                stroke(255,0,0);
                noFill();
            } else {
                stroke(0,255,0);
                noFill();
            }
            ellipse( 0, 0, myShock.size, myShock.size );
            translate(-myShock.x, 0);
            fill(255);
            stroke(255);
        }

        beginShape();        
        for(var i = 0; i < exp5.nPoints; i++){
            //ellipse(strings[stringID].points[i].x, strings[stringID].points[i].y, 2, 2);
            curveVertex(strings[stringID].points[i].x, strings[stringID].points[i].y);
        }
        endShape();

        translate(0, -strings[stringID].y);
    }
}


//interaction
function mouseClicked() {
  addShock(floor(random(strings.length)), 1);
}