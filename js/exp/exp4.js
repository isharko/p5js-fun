var sphere = new Sphere3D();
var html5radius = 50;
var html5direction = 0.1;
var myRotation = 0;
var distance = 0;
var fft;

function initExp4(myTune){
    fft = new p5.FFT();
}

function updateExp4() {
    frequencySpectrum = fft.analyze();
    background(0);
    drawExp4();
}

function Point3D() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
}

function Sphere3D(radius) {
    this.point = new Array();
    this.color = "rgb(100,0,255)"
    this.radius = (typeof(radius) == "undefined") ? 20.0 : radius;
    this.radius = (typeof(radius) != "number") ? 20.0 : radius;
    this.numberOfVertexes = 0;

    for(alpha = 0; alpha <= 6.28; alpha += 0.17) {
        p = this.point[this.numberOfVertexes] = new Point3D();
        
        p.x = Math.cos(alpha) * this.radius;
        p.y = 0;
        p.z = Math.sin(alpha) * this.radius;

        this.numberOfVertexes++;
    }

    for(var direction = 1; direction >= -1; direction -= 2) {
        for(var beta = 0.17; beta < 1.445; beta += 0.17) {
            var radius = Math.cos(beta) * this.radius;
            var fixedY = Math.sin(beta) * this.radius * direction;

            for(var alpha = 0; alpha < 6.28; alpha += 0.17) {
                p = this.point[this.numberOfVertexes] = new Point3D();

                p.x = Math.cos(alpha) * radius;
                p.y = fixedY;
                p.z = Math.sin(alpha) * radius;

                this.numberOfVertexes++;
            }
        }
    }
}

function rotateByX(myPoint, radians) {
    var y = myPoint.y;
    myPoint.y = (y * Math.cos(radians)) + (myPoint.z * Math.sin(radians) * -1.0);
    myPoint.z = (y * Math.sin(radians)) + (myPoint.z * Math.cos(radians));
}

function rotateByY(myPoint, radians) {
    var x = myPoint.x;
    myPoint.x = (x * Math.cos(radians)) + (myPoint.z * Math.sin(radians) * -1.0);
    myPoint.z = (x * Math.sin(radians)) + (myPoint.z * Math.cos(radians));
}

function rotateByZ(myPoint, radians) {
    var x = myPoint.x;
    myPoint.x = (x * Math.cos(radians)) + (myPoint.y * Math.sin(radians) * -1.0);
    myPoint.y = (x * Math.sin(radians)) + (myPoint.y * Math.cos(radians));
}

function projection(xy, z, xyOffset, zOffset, distance) {
    return ((distance * xy) / (z - zOffset)) + xyOffset;
}


function drawNoise(){

    for(i = 0; i < sphere.numberOfVertexes; i++) {

    }
}

//draw
function drawExp4() {    
   // drawNoise();
   var noiseVal;
   var noiseScale = 0.2;
   background(0);
    var x, y;
    var p = new Point3D();
    for(i = 0; i < sphere.numberOfVertexes; i++) {
        p.x = sphere.point[i].x;
        p.y = sphere.point[i].y;
        p.z = sphere.point[i].z;

        rotateByX(p, myRotation);
        rotateByY(p, myRotation);
        rotateByZ(p, myRotation);
        // noiseDetail(1,.1);
        if (p.z > 10){
            // fill(255,0,0);
        } else {
            // fill(255);
        }

        var h = map(frequencySpectrum[i], 0, 255, 0, height);


        x = projection(p.x, p.z, width/2.0, 100.0, distance + h );
        y = projection(p.y, p.z, height/2.0, 100.0, distance + h );


        if((x >= 0) && (x < width)) {
            if((y >= 0) && (y < height)) {
                if(p.z < 0) {
                    drawPoint(x, y, 1);
                } else {
                    drawPoint(x, y, 2);
                }
            }
        }                   
    }

    myRotation += (Math.PI )/990.0 ;
    myRotation += (mouseX-windowWidth/2 + mouseY-windowHeight/2)/100000;
    if(distance < windowHeight) {        
        distance += 10;
    }
}

function drawPoint(x, y, size, color) {
    fill(255);
    noStroke();
    ellipse(x,y, size, size);
}