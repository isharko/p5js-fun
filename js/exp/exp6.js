var exp6 = {
    quadStrips  : [],
    nStrips         : 25,
    stepWidth       : 50,
    nPoints         : 20,
    nQuads          : 30,
    alreadyInited   : 0
}

function Point3D() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.offsetX = 0;
    this.offsetY = 0;
    this.size = 5;
    this.destinationX = 0;
    this.destinationY = 0; 
    this.momentumX = 0;
    this.momentumY = 0;
    this.momentumDamp = random(0.1, 0.2);
}

function initExp6(myTune){
    if( exp6.alreadyInited == 1 ){ return; };
    fft = new p5.FFT();

    for (var i = 0; i < exp6.nStrips; i++){
        exp6.quadStrips[i] = new quadStrip();        
    }
    exp6.alreadyInited = 1;
}

function updateExp6() {
    frequencySpectrum = fft.analyze();
    background(0);

    for (var strip = 0; strip < exp6.quadStrips.length; strip++){
      var mStrip = exp6.quadStrips[strip];
      //cycle through quads
      for (var quad=0; quad < mStrip.quads.length; quad++){
        var mQuad = mStrip.quads[quad];        
        var h = map(frequencySpectrum[quad], 0, 255, 0, 255);

        //add colorOffset to the quad based on music
        mQuad.colorOffset = 250-h*h/1000;

        //cycle through first half of points in the quad
        for (var point=0; point < mQuad.points.length/2; point++){  
          var mPoint = mQuad.points[point];
          var dx = (mouseX-windowWidth/2) - mPoint.x;
          var dy = (mouseY-windowHeight/2) - mPoint.y;
          var dSqrd = dx*dx + dy*dy;
          var d = sqrt(dSqrd) + h*20.0;
          mPoint.x += d / 2000.0 * norm(dx, -1, 1);
          mPoint.y += d / 2000.0 * norm(dy, -1, 1);
          //mouse interactions            
          updatePoint(mPoint);          
        }
        //cycle through last half in the quad
        for (var point=mQuad.points.length/2; point < mQuad.points.length; point++){  
          h = map(frequencySpectrum[quad+1], 0, 255, 0, 255);
          if (quad == exp6.nQuads-1){h = map(frequencySpectrum[0], 0, 255, 0, 255)};
          
          var mPoint = mQuad.points[point];
          var dx = (mouseX-windowWidth/2) - mPoint.x;
          var dy = (mouseY-windowHeight/2) - mPoint.y;
          var dSqrd = dx*dx + dy*dy;
          var d = sqrt(dSqrd) + h*20.0;
          mPoint.x += d / 2000.0 * norm(dx, -1, 1);
          mPoint.y += d / 2000.0 * norm(dy, -1, 1);
          //mouse interactions            
          updatePoint(mPoint);          
        }
      }
    }
    drawExp6();
}

function updatePoint(p){
    var dX = p.x - p.destinationX;
    var dY = p.y - p.destinationY;

    //momentum
    p.momentumX -= dX/p.size;
    p.momentumY -= dY/p.size;

    //momentum dim
    p.momentumX *= 0.8;
    p.momentumY *= 0.8;    

    p.x += p.momentumX; // +momentum + funky mouse displacement
    p.y += p.momentumY; // +momentum + funky mouse displacement
}


//draw
function drawExp6() {      
  background(0);    

  fill(255);
  stroke(255);

  translate(windowWidth/2, windowHeight/2);
  noStroke();
  //cycle through strips
  for (var strip = 1; strip < exp6.quadStrips.length; strip++){
    var mStrip = exp6.quadStrips[strip];    
    //cycle through quads
    for (var quad=0; quad < mStrip.quads.length; quad++){
      beginShape();      
      var mQuad = mStrip.quads[quad];
      fill(mStrip.color - mQuad.colorOffset);      
      //cycle through points
      for (var point=0; point < mQuad.points.length; point++){  
        var mPoint = mQuad.points[point];
        vertex(mPoint.x + mPoint.offsetX, mPoint.y + mPoint.offsetY);
      }
      endShape(CLOSE);
      fill(255);
    }
  }
}   

function quadStrip(){
  this.id = exp6.quadStrips.length;
  this.color = ((this.id+1) % 2 ) * 255;
  this.colorOffset = 0;
  this.colorDir = 1;
  this.quads = [];
  this.angle = 0;
  this.theta = Math.PI * 2 / exp6.nQuads;
  var radOutsideX  = this.id * exp6.stepWidth;
  var radOutsideY  = this.id * (exp6.stepWidth - 10);
  var radInsideX   = radOutsideX-exp6.stepWidth;
  var radInsideY   = radOutsideY-(exp6.stepWidth - 10);

  for ( i = 0; i < exp6.nQuads; i++){
    this.quads[this.quads.length] = new mQuad(this.quads, i, this.angle, this.theta, radInsideX, radInsideY, radOutsideX, radOutsideY);
    // addQuad(this.quads, i, this.angle, this.theta, radInside, radOutside);
    this.angle += this.theta;
  }
}

function mQuad(quads, index, mAngle, mTheta, radInsideX, radInsideY, radOutsideX, radOutsideY){  
  var p1,p2,p3,p4;
  p1 = new Point3D();
  p2 = new Point3D();
  p3 = new Point3D();
  p4 = new Point3D();
  this.points = [];
  p1.x = radInsideX * cos(mAngle - mTheta/2);
  p1.y = radInsideY * sin(mAngle - mTheta/2);

  p2.x = radOutsideX * cos(mAngle - mTheta/2);
  p2.y = radOutsideY * sin(mAngle - mTheta/2);

  p3.x = radOutsideX * cos(mAngle + mTheta/2);
  p3.y = radOutsideY * sin(mAngle + mTheta/2);

  p4.x = radInsideX * cos(mAngle + mTheta/2);
  p4.y = radInsideY * sin(mAngle + mTheta/2);

  setDestination(p1);
  setDestination(p2);
  setDestination(p3);
  setDestination(p4);

  this.points[this.points.length] = p1;
  this.points[this.points.length] = p2;
  this.points[this.points.length] = p3;
  this.points[this.points.length] = p4;
  // quads[quads.length] = points;
}

function setDestination(p){
  p.destinationX = p.x;
  p.destinationY = p.y;
}