var exp6 = {
    quadStrips  : [],
    nStrips         : 20,
    stepWidth       : 50,
    nPoints         : 20,
    nQuads          : 30
}

function Point3D() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.offsetX = 0;
    this.offsetY = 0;
    this.size = 5;
    this.destinationX = 0;
    this.destinationY = 0; 
    this.momentumX = 0;
    this.momentumY = 0;
    this.momentumDamp = random(0.1, 0.2);
}

function initExp6(myTune){
    fft = new p5.FFT();
    for (var i = 0; i < exp6.nStrips; i++){
      exp6.quadStrips[i] = new quadStrip(i);        
    }
}

function updateExp6() {
    frequencySpectrum = fft.analyze();
    background(0);
    var h = map(frequencySpectrum[20], 0, 255, 0, 255);
    
    drawExp6();
}

function updatePoint(p){
    var dX = p.x - p.destinationX;
    var dY = p.y - p.destinationY;

    //momentum
    p.momentumX -= dX/p.size;
    p.momentumY -= dY/p.size;

    //momentum dim
    p.momentumX *= 0.8;
    p.momentumY *= 0.8;    

    p.x += p.momentumX; // +momentum + funky mouse displacement
    p.y += p.momentumY; // +momentum + funky mouse displacement
}


//draw
function drawExp6() {      
  background(0);
  translate(windowWidth/2, windowHeight/2);
  fill(255);
  strokeWeight(exp6.stepWidth);

  var stripeColor = "white";
  
  for (var i = 0; i < exp6.quadStrips.length; i++){
    var strip = exp6.quadStrips[i];
    if ((i+1) % 2 == 0){
      stripeColor = "white";
    } else {
      stripeColor = "black";      
    }

    
    for (var j = 1; j <= strip.points.length; j++){
      var p1, p2;
      if( j == strip.points.length ){
        p1 = strip.points[j-1];
        p2 = strip.points[0];
      } else {
        p1 = strip.points[j-1];
        p2 = strip.points[j];
      }

      if (stripeColor == "black"){
        stroke(0);
        line(p1.x, p1.y, p2.x, p2.y);
      } else {
        stroke(255-random(0,100));
        line(p1.x, p1.y, p2.x, p2.y);
        // var c1 = color (255,255,255);
        // var c2 = color (200,200,200);
        // gradient(p1,p2, c1,c2);
      }
      // ellipse(p1.x, p1.y, 10, 10);      
      
    }
  }

  fill(255);
  stroke(255);

}   

function gradient(sp1, sp2, c1, c2){
  var dx, dy;
  dx = sp1.x - sp2.x;
  dy = sp1.y - sp2.y;
  var dSqrd = Math.sqrt(dx*dx + dy*dy);

  var a = atan2(dx,dy);
}

function quadStrip(index){
  var theta = Math.PI * 2.0 / exp6.nPoints;
  var angle = 0.0;
  this.points = [];
  var radius = exp6.stepWidth * (index+1);
  for (var i = 0; i < exp6.nPoints; i ++){
    var p = new Point3D();
    p.x = radius * cos(angle);
    p.y = radius * sin(angle);
    p.destinationX = p.x;
    p.destinationY = p.y;
    this.points[this.points.length] = p;
    angle += theta;
  }
}