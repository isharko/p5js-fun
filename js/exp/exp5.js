var exp5 = {
    xspacing    : 16,
    theta       : 0.0,
    amplitude   : 75.0,
    period      : 500.0,   // How many pixels before the wave repeats
    yValues     : 0,
    nStrings    : 15,
    dx          : 0,
    ySpacing    : 20,
    nPoints     : 50,
    effector    : effector()    
}

var strings = [];
var waves   = [];

function initExp5(myTune){
    fft = new p5.FFT();
    // exp5.dx = (TWO_PI / exp5.period) * exp5.xspacing;
    exp5.xspacing = windowWidth / exp5.nPoints;    
    for (var i = 0; i < exp5.nStrings; i++){
        strings[i] = new string( i );
    }
}

//string
function string(i){
    this.id         = i;    
    this.y          = (i - exp5.nStrings/2) * exp5.ySpacing + windowHeight/2;
    this.points     = [];
    this.shock      = 0;
    this.shockDim   = 0.9;
    this.momentum   = 0;
    this.shocks     = [];
    this.color      = color(255);

    for(var i = 0; i < exp5.nPoints; i++){
        var p = new _point();
        p.x = exp5.xspacing * i;
        this.points[i] = p;
        p.destinationX = p.x;
        p.destinationY = 0;
    }

}

function _point(){
    this.x = 0;
    this.y = 0;
    this.size = random(5,10);
    this.destinationX = 0;
    this.destinationY = 0; 
    this.momentumX = 0;
    this.momentumY = 0;
    this.momentumDamp = random(0.1, 0.2);
}


function effector(){
    this.sizeX = 30;
    this.sizeY = 10;
}

function updateEffector(){
    var h = map(frequencySpectrum[20], 0, 255, 0, 255);
    effector.sizeX = h;
    effector.sizeY = h*2;
}

function drawEffector(){
    stroke(0, 100, 100);
    noFill();
    ellipse(mouseX, mouseY, effector.sizeX, effector.sizeY);
}



//update
function updateExp5() {
    frequencySpectrum = fft.analyze();
    background(0);
    updateEffector();    
    updateStrings();
    drawExp5();
}

function updateStrings(){
    //find  dY, then dX;
    for (var s = 0; s < strings.length; s++){
        var dY = strings[s].y - mouseY;
        if (abs(dY) < effector.sizeY){
            for ( var i = 0; i < strings[s].points.length; i++ ){
                var p = strings[s].points[i];
                var dX = p.x - mouseX;
                // if a point should be offset by the effector
                if ( abs(dX) < effector.sizeX ){
                    p.y += min(10,(effector.sizeY*(dX/500) * (dY)) / 100) ;
                }
            }
        } else {
            strings[s].color = color(255);
        }

        for ( var i = 0; i < strings[s].points.length; i++ ){
            updatePoint( strings[s].points[i] );                
        }
    }
}

function updatePoint(p){
    var dX = p.x - p.destinationX;
    var dY = p.y - p.destinationY;

    //momentum
    p.momentumX -= dX/p.size;
    p.momentumY -= dY/p.size;

    //momentum dim
    p.momentumX *= 0.8;
    p.momentumY *= 0.8;
    

    p.x += p.momentumX; // +momentum + funky mouse displacement
    p.y += p.momentumY; // +momentum + funky mouse displacement
}


//draw
function drawExp5() {        
    drawStrings();
}

//draw string
function drawStrings(){
    stroke(255);
    // drawEffector();

    //foreach string
    for (var stringID = 0; stringID < strings.length; stringID++){
        translate(0, strings[stringID].y);

        stroke(strings[stringID].color);
        noFill();
        
        beginShape();        
        for(var i = 0; i < exp5.nPoints; i++){
            curveVertex(strings[stringID].points[i].x, strings[stringID].points[i].y);
        }
        endShape();

        translate(0, -strings[stringID].y);
    }
}


//interaction
function mouseClicked() {
}