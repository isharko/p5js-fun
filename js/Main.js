var curExp 		= 0;
var lerpTiming 	= 150;
var time		= 0;
var globalTime	= 0;
var LERP_ON		= true;

//preload
function preload(){
	myTune = loadSound('assets/sound/boom.mp3');
}

//setup
function setup(){
	createCanvas(windowWidth, windowHeight);
	initExp();
	myTune.play(1,1);
    masterVolume(1.0);
}


//draw
function draw(){

	switch(curExp){
		case 0:
			updateExp0();
			break;
		case 1:
			updateExp1();	
			break;
		case 2:
			updateExp2(myTune);	
			break;
		case 3:
			updateExp3(myTune);	
			break;
		case 4:
			updateExp4(myTune);	
			break;
		case 5:
			updateExp5(myTune);	
			break;
		case 6:
			updateExp6(myTune);	
			break;
			
	}

	//Lerper
	time++;
	if (time > lerpTiming){
		lerpExp();
		time = 0;
	}

	drawLogo();
	globalTime += 0.01;
}

function lerpExp(){
	if (!LERP_ON) return;
	curExp++;
	if (curExp == 10){
		curExp = 0;
	}
	initExp();
}


//initiate a new visual experiment

function initExp(){
	switch(curExp){
		case 0:
			initExp0();
			break;
		case 1:
			initExp1();
			break;	
		case 2:
			initExp2(myTune);
			break;	
		case 3:
			initExp3(myTune);
			break;	
		case 4:
			initExp4(myTune);
			break;				
		case 5:
			initExp5(myTune);
			break;
		case 6:
			initExp6(myTune);
			break;	
	
	}
}

// key pressed
function keyPressed() {
	console.log(keyCode);
  if (keyCode >= 48 && keyCode <= 70){
  	curExp = keyCode - 48;
  	initExp();
  }
  if (keyCode == 76){ // l
  	LERP_ON = !LERP_ON;
  }
}
